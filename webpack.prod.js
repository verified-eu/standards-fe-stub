const path = require("path");
const { merge } = require("webpack-merge");

const common = require("./webpack.common.js");

module.exports = merge(common, {
  mode: "production",
  devtool: "inline-source-map",
  optimization: {
    runtimeChunk: "single",
    minimize: true,
    namedModules: false,
    namedChunks: false,
    moduleIds: "hashed",
    nodeEnv: "production",
    usedExports: true,
  },
});
