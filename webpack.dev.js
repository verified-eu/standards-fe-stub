const Path = require("path");
const Webpack = require("webpack");

const { merge } = require("webpack-merge");

const common = require("./webpack.common.js");

const devServerHost = "0.0.0.0";
const devServerPort = 8081;


module.exports = merge(common, {
  mode: "development",
  devtool: "eval-source-map",

  devServer: {
    host: devServerHost,
    port: devServerPort,

    watchOptions: {
      poll: 200
    },

    inline: true,
    hot: true,
    historyApiFallback: true,
    disableHostCheck: true,
    serveIndex: true,

    before: function (app, webpackServer) {
      // We override the listen() function to set keepAliveTimeout.
      // See: https://github.com/microsoft/WSL/issues/4340
      // Original listen(): https://github.com/webpack/webpack-dev-server/blob/f80e2ae101e25985f0d7e3e9af36c307bfc163d2/lib/Server.js#L744
      const { listen } = webpackServer;
      webpackServer.listen = function (...args) {
        const server = listen.call(this, ...args);
        server.keepAliveTimeout = 0;
        return server;
      };
    },
  },

  plugins: [
    new Webpack.HotModuleReplacementPlugin()
  ]
});
