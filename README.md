[//]: # (block-start:title)

# Code 11 Standards - Front-End Stub Project (DELETE)

[//]: # (block-stop:title)

[//]: # (block-start:summary)

[//]: # (block-stop:summary)


### Quickstart
````
git clone git@bitbucket.org:verified-eu/standards-fe-stub.git

npm install
npm start
````

A development server located at http://localhost:8081 will welcome you.
While working locally, hot module replacement is enabled

### Production building

````
npm run build

# In case you want to serve the build result
npm run serve
````

When serving the production build, the application will be available at http://localhost:5000

### Authoring standalone apps

### Authoring single-spa 

### Authoring modules