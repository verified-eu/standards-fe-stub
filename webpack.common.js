const Fs = require("fs");
const Path = require("path");

const HtmlWebpackPlugin = require("html-webpack-plugin");

const folderDist = Path.resolve(__dirname, "dist");
const folderSrc = Path.resolve(__dirname, "src");
const folderLib = Path.resolve(folderSrc, "lib");

const fileBabelRC = Path.resolve(__dirname, "./.babelrc");

module.exports = {
  entry: {
    app: "./src/app.js",
  },
  output: {
    publicPath: "/",
    path: folderDist,
    filename: "[name].js",
    chunkFilename: "[name].js",
  },
  resolve: {
    modules: [folderSrc, folderLib, "node_modules"],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx|ts|tsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
            options: Object.assign(
              {},
              JSON.parse(Fs.readFileSync(fileBabelRC)),
              {
                cacheDirectory: true,
                env: {
                  development: {
                    comments: true,
                  },
                  production: {
                    comments: true,
                    minified: true,
                    plugins: [],
                  },
                },
              }
            ),
          },
        ],
      },
      {
        test: /src\/.+\.html$/,
        use: [
          {
            loader: "html-loader",
            options: {
              minimize: true
            },
          },
        ],
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]?[hash]",
              outputPath: "assets",
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "src/index.html",
      filename: "index.html",
      favicon: "src/assets/favicon.png",
    }),
  ],
};
