async function main() {
  const { Module } = await import(/* webpackChunkName: "module" */ "module.ts");

  Module.bootstrap();
  Module.mount();

  window.onunload = () => Module.unmount();
}

main()
  .then(() => {
    console.log(`Bootstrap complete`);
  })
  .catch((err) => {
    console.error(err);
  });