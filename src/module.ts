import { IModule } from "./module.interface";

const Bootstrap = (config: any): Promise<void> => {
  console.log(`Bootstrapping module`);
  return Promise.resolve();
};

const Mount = (config: any): any => {
  console.log(`Mounting module`);
  return Promise.resolve();
};

const Unmount = (config: any): any => {
  console.log(`Unmounting module`);
  return Promise.resolve();
};

export const Module: IModule = {
  bootstrap: Bootstrap,
  mount: Mount,
  unmount: Unmount,
};

console.log(`Module loaded!`);