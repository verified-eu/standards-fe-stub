export interface IModule {
  bootstrap: (...args: any[]) => Promise<void>;
  mount: (...args: any[]) => any;
  unmount: (...args: any[]) => any;
}

export const A = 10;